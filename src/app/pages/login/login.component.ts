import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {NgForm} from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnChanges {
  username:string = 'what' ;
  password:string;

  constructor(private router: Router) { }

  ngOnChanges(changes:any){
    console.log(changes, this.username)
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm){
    this.username = form.value.username;
    this.password = form.value.password;
    this.router.navigate(['../todo', { id: 1 }])
    if(this.username === 'admin' && this.password === 'admin'){
      this.router.navigateByUrl('/todo', { skipLocationChange: true });
    }
  }
}
