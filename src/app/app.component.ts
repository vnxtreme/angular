import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  cart: Array<any> = [];
  title = "first-project";
  products: Array<Object> = [
    {
      id: 1,
      name: "Licensed Frozen Hat",
      description: "Incidunt et magni",
      price: "170.00",
      quantity: 1
    },
    {
      id: 2,
      name: "Rustic Concrete Chicken",
      description: "Sint libero mollitia",
      price: "302.00",
      quantity: 1
    },
    {
      id: 3,
      name: "Fantastic Metal Computer",
      description: "In consequuntur cupiditat",
      price: "279.00",
      quantity: 1
    },
    {
      id: 4,
      name: "Refined Concrete Chair",
      description: "Saepe nemo praesentium",
      price: "760.00",
      quantity: 1
    }
  ];

  handleAddCart(product) {
    let prodIndex = this.cart.findIndex((prod, index) => {
      return prod.id === product.id;
    })
    
    if(prodIndex > -1){
			this.cart[prodIndex].quantity += 1;
    } else {
			this.cart.push(product)
		}
	}
	
	handleDeleteProduct(prodId){
		this.cart = this.cart.filter(item => item.id !== prodId);
	}
}
