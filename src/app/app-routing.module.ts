import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TodoComponent} from './pages/todo/todo.component';

const routes: Routes = [
  {path:'todo', component: TodoComponent}
  // {path: 'todo', loadChildren: () => import('./pages/todo/todo.component').then(m => m.TodoComponent)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
