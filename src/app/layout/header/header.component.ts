import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnChanges {
	isCartActive: boolean = false;
	totalAmount: Number = 0;
	totalProduct: Number = 0;
	@Input() cart: Array<Object> = [];
	@Output() emitDeleteProduct = new EventEmitter();

	constructor() { }

  ngOnInit() {
		this.totalAmount = this.calculateTotalAmount(this.cart);
  }

  cartOnclick(){
    this.isCartActive = !this.isCartActive;
	}

	calculateTotalAmount(cart: Array<any>){
		return cart.reduce((item, total) => total + item.price, 0)
	}

	calculateTotalProduct(cart: Array<any>){
		return cart.reduce((item, total) => total + item.quantity, 0)
	}

	handleDelete(prodId){
		this.emitDeleteProduct.emit(prodId);
	}
	
	ngOnChanges(changes: SimpleChanges){
		this.totalAmount = this.calculateTotalAmount(changes.cart.currentValue);
		this.calculateTotalProduct = this.calculateTotalProduct(changes.cart.currentValue);
	}
}
